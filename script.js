$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    // Khai báo biến toàn cục để lưu trữ combo được chọn mỗi khi khách chọn lại đổi giá trị properties của nó
    var gSelectedMenuStructure = {
        menuName: "",    // S, M, L
        duongKinhCM: "",
        suongNuong: "",
        saladGr: "",
        drink: "",
        priceVND: ""
    };
    // Khai báo biến toàn cục để lưu loại pizza đươc chọn, mỗi khi khách chọn lại đổi giá trị cho nó
    var gSelectedPizzaType = "";
    // Khai báo biến toàn cục để lưu phần trăm giảm giá của voucher
    var gPercent = 0;

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    // Gán event handler cho nút  Chọn trên commbo S
    $('#btn-combo-small').on('click', function(){
        onBtnComboSmallClick();
    });
    // Gán event handler cho nút  Chọn trên commbo M
    $('#btn-combo-medium').on('click', function(){
        onBtnComboMediumClick();
    });
    // Gán event handler cho nút  Chọn trên commbo L
    $('#btn-combo-large').on('click', function(){
        onBtnComboLargeClick();
    });
    // Gán event handler cho nút Chọn trên type Ocean Mania
    $('#btn-type-oceanmania').on('click', function(){
        onBtnTypeOceanManiaClick();
    });
    // Gán event handler cho nút Chọn trên type Hawaii
    $('#btn-type-hawaii').on('click', function(){
        onBtnTypeHawaiiClick();
    });
    // Gán event handler cho nút Chọn trên type Bacon
    $('#btn-type-bacon').on('click', function(){
        onBtnTypeBaconClick();
    });
    // Gán event handler cho nút Gửi đơn hàng
    $('#btn-gui-don-hang').on('click', function(){
        onBtnGuiDonHangClick();
    });
    // Gán event handler cho nút Tạo đơn hàng trên modal
    $('#btn-create-order').on('click', function(){
        onBtnTaoDonHangClick();
    });
    // Gán event handler cho nút đóng trên modal khi tạo đơn hàng xong sẽ load lại trang
    $('#btn-dismiss-modal').on('click', function(){
        location.reload(true);
    })

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm gọi khi load trang
    function onPageLoading() {
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: 'GET',
            dataType: 'json',
            success: function(res){
                console.log(res);
                loadDataDrinkToSelect(res);
            },
            error: function(error){
                alert(error.responseText);
            }
        });     
    }
    // Hàm thực thi khi combo S được click
    function onBtnComboSmallClick() {
        "use strict";
        // đổi màu button
        changeComboButtonColor("Small");
        //tạo một đối tượng gói dịch vụ, được tham số hóa
        var vSelectedCombo = getCombo("S (small)", "20 cm", 2,"200 g", 2, 150000);
        // gọi method hiển thị thông tin
        vSelectedCombo.displayInConsoleLog();
        // gán giá trị của combo được chọn vào biến toàn cục 
        gSelectedMenuStructure = vSelectedCombo;
    }
    // Hàm thực thi khi combo M được click
    function onBtnComboMediumClick() {
        "use strict";
        // đổi màu button
        changeComboButtonColor("Medium");
        //tạo một đối tượng gói dịch vụ, được tham số hóa
        var vSelectedCombo = getCombo("M (medium)", "25 cm", 4,"300 g", 3, 200000);
        // gọi method hiển thị thông tin
        vSelectedCombo.displayInConsoleLog();
        // gán giá trị của combo được chọn vào biến toàn cục 
        gSelectedMenuStructure = vSelectedCombo;
    }
    // Hàm thực thi khi combo L được click
    function onBtnComboLargeClick() {
        "use strict";
        // đổi màu button
        changeComboButtonColor("Large");
        //tạo một đối tượng gói dịch vụ, được tham số hóa
        var vSelectedCombo = getCombo("L (large)", "30 cm", 8,"500 g", 4, 250000);
        // gọi method hiển thị thông tin
        vSelectedCombo.displayInConsoleLog();
        // gán giá trị của combo được chọn vào biến toàn cục
        gSelectedMenuStructure = vSelectedCombo;
    }
    // Hàm thực thi khi type OCEAN MANIA được click
    function onBtnTypeOceanManiaClick() {
        "use strict";
        // đổi màu button
        changePizzaTypeButtonColor("Ocean Mania");
        //tạo một đối tượng gói dịch vụ, được tham số hóa
        var vSelectedType = getTypePizza("Ocean Mania");
        // gọi method hiển thị thông tin
        vSelectedType.displayInConsoleLog();
        // gán giá trị của combo được chọn vào biến toàn cục để lưu tại đó
        gSelectedPizzaType = vSelectedType;
    }
    // Hàm thực thi khi type HAWAII được click
    function onBtnTypeHawaiiClick() {
        "use strict";
        // đổi màu button
        changePizzaTypeButtonColor("Hawaii");
        //tạo một đối tượng gói dịch vụ, được tham số hóa
        var vSelectedType = getTypePizza("Hawaii");
        // gọi method hiển thị thông tin
        vSelectedType.displayInConsoleLog();
        // gán giá trị của combo được chọn vào biến toàn cục để lưu tại đó
        gSelectedPizzaType = vSelectedType;
    }
    // Hàm thực thi khi type BACON được click
    function onBtnTypeBaconClick() {
        "use strict";
        // đổi màu button
        changePizzaTypeButtonColor("Bacon");
        //tạo một đối tượng gói dịch vụ, được tham số hóa
        var vSelectedType = getTypePizza("Bacon");
        // gọi method hiển thị thông tin
        vSelectedType.displayInConsoleLog();
        // gán giá trị của combo được chọn vào biến toàn cục để lưu tại đó
        gSelectedPizzaType = vSelectedType;
    }
    // Hàm thực thi khi ấn nút Gửi đơn hàng
    function onBtnGuiDonHangClick() {
        "use strict";
        console.log("%cNút Gửi đơn hàng được ấn: ", "color:red");
        gPercent = 0; // mỗi khi ấn nút thì cho biến toàn cục trở về giá trị 0 ban đầu
        //Bước 0: khai báo biến lưu đối tượng dữ liệu
        var vThongTinDonHang = {
            comboDuocChon: "",
            loaiPizza: "",
            doUong: "",
            hoVaTen: "",
            email: "",
            dienThoai: "",
            diaChi: "",
            loiNhan: "",
            voucher: "",
        };
        //Bước 1. thu thập dữ liệu
        getDataFromForm(vThongTinDonHang);
        //Bước 2. Kiểm tra dữ liệu
        var vCheck = validateData(vThongTinDonHang);
        console.log(vCheck);
        if(vCheck == true){
            if (vThongTinDonHang.voucher == ""){ // nếu ko nhập vào ô voucher thì ko cần gọi API
                $('#order-modal').modal();
                displayDataOrderToModal(vThongTinDonHang);
            } 
            else { // ngược lại nếu ô voucher có dữ liệu thì gọi API để kiểm tra voucher
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + vThongTinDonHang.voucher,
                    type: 'GET',
                    dataType: 'json',
                    success: function(res){
                        console.log(res);
                        gPercent = res.phanTramGiamGia;
                        console.log("Phần trăm giảm giá: " + gPercent + " %");
                        $('#order-modal').modal();
                        displayDataOrderToModal(vThongTinDonHang);
                    },
                    error: function(error){
                        alert(error.responseText);
                    }
                });
            }
        } 
    } 
    // Hàm thực thi khi nhấn nút Tạo đơn hàng  trên modal
    function onBtnTaoDonHangClick(){
        "use strict";
        console.log("%cNút Tạo đơn hàng được ấn: ", "color:red");
        $('#order-modal').modal('hide');
        //Bước 0: khai báo object order để chứa thông tin đặt hàng 
        var vDataOrder = {
            hoTen: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: "",  
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            loaiPizza: "",
            idVourcher: "",
            thanhTien: "",      
        };
        //Bước 1: thu thập dữ liệu
        getDataFromFormToCreatOrder(vDataOrder);
        console.log(vDataOrder);
        //Bước 2: validate (ko cần vì đã validate ở nút Gửi đơn hàng)
        //Bước 3: Gọi API để tạo đơn hàng
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "POST",
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(vDataOrder),
            success: function(res){
                console.log(res);
                displayOrderIdToModal(res);
                $('#confirm-order-modal').modal();
            },
            error: function(error){
                alert(error.responseText);
            }
        });        
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // hàm đổi màu nút Goi Combo được chọn
    function changeComboButtonColor(paramCombo) {
        "use strict";
        var vElementBtnSmall = $('#btn-combo-small');
        var vElementBtnMedium = $('#btn-combo-medium');
        var vElementBtnLarge = $('#btn-combo-large');
        if (paramCombo === "Small") {
            vElementBtnSmall.removeClass().addClass('btn btn-success border border-warning fontComic w-100');
            vElementBtnMedium.removeClass().addClass('btn btn-warning fontComic w-100');
            vElementBtnLarge.removeClass().addClass('btn btn-warning fontComic w-100');
        }
        else if (paramCombo === "Medium") {  
            vElementBtnSmall.removeClass().addClass('btn btn-warning fontComic w-100');
            vElementBtnMedium.removeClass().addClass('btn btn-success border border-warning fontComic w-100');
            vElementBtnLarge.removeClass().addClass('btn btn-warning fontComic w-100');
        }
        else if (paramCombo === "Large") { 
            vElementBtnSmall.removeClass().addClass('btn btn-warning fontComic w-100');
            vElementBtnMedium.removeClass().addClass('btn btn-warning fontComic w-100');
            vElementBtnLarge.removeClass().addClass('btn btn-success border border-warning fontComic w-100');
        }
    }
    //function get Combo (lấy ra dữ liệu combo)
    function getCombo(paramCombo, paramDuongKinh, paramSuonNuong, paramSalad, paramNuocNgot, paramGiaVND) {
        "use strict";
        var vSelectedCombo = {  
            combo: paramCombo, 
            duongkinh: paramDuongKinh,
            suonnuong: paramSuonNuong, 
            salad: paramSalad,
            nuocngot: paramNuocNgot,
            giavnd: paramGiaVND,
            displayInConsoleLog() {
                console.log("%cCOMBO SELECTED - %cgói combo pizza được chọn...", "color:red", "color:green");
                console.log("- " + this.combo); 
                console.log("- Đường kính: " + this.duongkinh);
                console.log("- Sườn nướng: " + this.suonnuong);
                console.log("- Salad: " + this.salad);
                console.log("- Nước ngọt: " + this.nuocngot);       
                console.log("- Giá: " + this.giavnd + " VNĐ");
            }
        };
        return vSelectedCombo;  //trả lại đối tượng, có đủ dữ liệu (attribute) và các methods (phương thức)
    }
    // hàm đổi màu nút Loại Pizza được chọn
    function changePizzaTypeButtonColor(paramType) {
        "use strict";
        var vElementBtnOceanMania = $('#btn-type-oceanmania');
        var vElementBtnHawaii = $('#btn-type-hawaii');
        var vElementBtnBacon = $('#btn-type-bacon'); 

        if (paramType === "Ocean Mania") {
            vElementBtnOceanMania.removeClass().addClass('btn btn-success border border-warning fontComic w-100');
            vElementBtnHawaii.removeClass().addClass('btn btn-warning fontComic w-100');
            vElementBtnBacon.removeClass().addClass('btn btn-warning fontComic w-100');
        }
        else if (paramType === "Hawaii") {  
            vElementBtnOceanMania.removeClass().addClass('btn btn-warning fontComic w-100');
            vElementBtnHawaii.removeClass().addClass('btn btn-success border border-warning fontComic w-100');
            vElementBtnBacon.removeClass().addClass('btn btn-warning fontComic w-100');
        }
        else if (paramType === "Bacon") { 
            vElementBtnOceanMania.removeClass().addClass('btn btn-warning fontComic w-100');
            vElementBtnHawaii.removeClass().addClass('btn btn-warning fontComic w-100');
            vElementBtnBacon.removeClass().addClass('btn btn-success border border-warning fontComic w-100');
        }
    }
    //function get Loại pizza
    function getTypePizza(paramType) {
        "use strict";
        var vSelectedType = {  
            type: paramType,
            displayInConsoleLog() {
                console.log("%cTYPE PIZZA SELECTED - %cloại pizza được chọn...", "color:red", "color:green");
                console.log("- Vị: " + this.type);  //this = "đối tượng này" 
            }
        };
        return vSelectedType;  //trả lại đối tượng, có đủ dữ liệu (attribute) và các methods (phương thức)
    }
    // Hàm load dữ liệu lên select drink khi tải trang
    function loadDataDrinkToSelect(paramResponse){
        "use strict";
        var vSelectDrink = $("#select-drink");
        for (var bI = 0; bI < paramResponse.length; bI++) {
            var bOption = $("<option/>", {
                text: paramResponse[bI].tenNuocUong,
                value: paramResponse[bI].maNuocUong
            }).appendTo(vSelectDrink);
        }
    }
    // Hàm thu thập dữ liệu từ web (bước 1 nút Kiểm tra đơn hàng)
    function getDataFromForm(paramObj){
        "use strict";
        paramObj.comboDuocChon = gSelectedMenuStructure.combo; 
        paramObj.loaiPizza = gSelectedPizzaType.type; 
        paramObj.doUong = $('#select-drink :selected').text();
        paramObj.hoVaTen = $('#inp-fullname').val().trim();    
        paramObj.email = $('#inp-email').val().trim();        
        paramObj.dienThoai = $('#inp-dien-thoai').val().trim();        
        paramObj.diaChi = $('#inp-dia-chi').val().trim();        
        paramObj.loiNhan = $('#inp-message').val().trim();        
        paramObj.voucher = $('#inp-voucher').val().trim();
    }
    // Hàm kiểm tra dữ liệu sau khi thu thập (Bước 2 nút Kiểm tra đơn hàng)
    function validateData(paramObj){
        "use strict";
        var vRegexStr = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        var vResult = true;
        if (paramObj.comboDuocChon == null) {
            vResult = false;
            alert("Hãy chọn Combo!");
        }
        else if (paramObj.loaiPizza == null) {
            vResult = false;
            alert("Hãy chọn loại Pizza!");
        }
        else if (paramObj.doUong == "Chọn..."){
            vResult = false;
            alert("Hãy chọn đồ uống!");
        }
        else if (paramObj.hoVaTen == "") {
            vResult = false;
            alert("Hãy nhập họ tên!");
        }
        else if (paramObj.email == ""){
            vResult = false;
            alert("Hãy nhập email!");
        }
        else if(!vRegexStr.test(paramObj.email)){
            alert("Email không hợp lệ");
            vResult = false;
        }
        else if (paramObj.dienThoai == "") {
            vResult = false;
            alert("Hãy nhập số điện thoại!");
        }
        else if(isNaN(paramObj.dienThoai)){
            vResult = false;
            alert("Số điện thoại phải là số!");
        }
        else if (paramObj.diaChi == "") {
            vResult = false;
            alert("Hãy nhập địa chỉ!");
        } 
        return vResult;
    }
    //hàm hiển thị dữ liệu lên vùng modal
    function displayDataOrderToModal(paramData){
        "use strict";
        $('#inp-fullname-modal').val(paramData.hoVaTen);
        $('#inp-phone-modal').val(paramData.dienThoai);
        $('#inp-address-modal').val(paramData.diaChi);
        if(paramData.loiNhan !== ""){
            $('#inp-message-modal').val(paramData.loiNhan);
        }
        else {
            $('#inp-message-modal').val("----");
        }
        if(paramData.voucher !== ""){
            $('#inp-idvoucher-modal').val(paramData.voucher);
        }
        else {
            $('#inp-idvoucher-modal').val("----");
        }        
        $('#textarea-modal').val(
            "Xác nhận: " + paramData.hoVaTen + ", " + paramData.dienThoai + ", " + paramData.diaChi + "." + '\n' 
            + "Menu " + paramData.comboDuocChon + ", sườn nướng " + gSelectedMenuStructure.suonnuong + ", salad " 
            + gSelectedMenuStructure.salad + ", " + gSelectedMenuStructure.nuocngot + " " + paramData.doUong + "." + '\n' 
            + "Loại pizza: " + paramData.loaiPizza + ". Giá: " + gSelectedMenuStructure.giavnd + " VNĐ." + '\n'
            + "Mã giảm giá: " + noneVoucher(paramData.voucher) + "." + '\n'
            + "Phải thanh toán: " + (gSelectedMenuStructure.giavnd - (gSelectedMenuStructure.giavnd * gPercent / 100)) + " VNĐ"
            + " (Giảm giá " + gPercent + "%)."
        );
    }
    // Hàm chuyển đổi giá trị của mã giảm giá khi không nhập mã
    function noneVoucher(paramDatavoucher){
        "use strict";
        if(paramDatavoucher == ""){
            return "(không có)"
        }
        return paramDatavoucher;
    }
    // Hàm thu thập dữ liệu (bước 1 nút Tạo đơn hàng)
    function getDataFromFormToCreatOrder(paramData){
        "use strict";        
        paramData.hoTen = $('#inp-fullname').val();        
        paramData.email = $('#inp-email').val();    
        paramData.soDienThoai = $('#inp-dien-thoai').val();
        paramData.diaChi = $('#inp-dia-chi').val();       
        paramData.loiNhan = $('#inp-message').val();
        paramData.kichCo = gSelectedMenuStructure.combo; 
        paramData.duongKinh = gSelectedMenuStructure.duongkinh;
        paramData.suon = gSelectedMenuStructure.suonnuong;
        paramData.salad = gSelectedMenuStructure.salad;        
        paramData.idLoaiNuocUong = $('#select-drink :selected').val();
        paramData.soLuongNuoc = gSelectedMenuStructure.nuocngot;
        paramData.loaiPizza = gSelectedPizzaType.type;
        paramData.idVourcher = $('#inp-voucher').val();
        paramData.thanhTien = (gSelectedMenuStructure.giavnd - (gSelectedMenuStructure.giavnd * gPercent / 100));
    }
    // hàm xử lý hiển thị sau khi gọi APi tạo đơn hàng thành công (Bước 4 Tạo đơn hàng)
    function displayOrderIdToModal(responseText){
        "use strict";
        $('#inp-madonhang-modal').val(responseText.orderId);
    }

});